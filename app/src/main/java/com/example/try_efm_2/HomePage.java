package com.example.try_efm_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

public class HomePage extends AppCompatActivity {

    public void out(View view)
    {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(HomePage.this,Registration.class));


    }
    // it was very nice experience //


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
    }
}
