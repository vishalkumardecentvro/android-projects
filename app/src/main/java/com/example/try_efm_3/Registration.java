package com.example.try_efm_3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Registration extends AppCompatActivity implements Animation.AnimationListener {
    EditText inputEmail;
    EditText inputPassword;
    Button next;
    TextView click,head;
    CardView details;
    Animation animBlink;
    int k=0;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        inputEmail = findViewById(R.id.inputEmail);
        inputPassword = findViewById(R.id.inputPassword);
        head = findViewById(R.id.headingTextView);
        next = findViewById(R.id.nextButtonView);
        click = findViewById(R.id.changeToLogin);

        mAuth = FirebaseAuth.getInstance();

        // following block is to check wether user is logged in or not, if it islogged in then it will open home page else registration page

        if(mAuth.getCurrentUser()!=null && mAuth.getCurrentUser().isEmailVerified())
        {

            startActivity(new Intent(Registration.this,HomePage.class));
            finish();
        }

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = inputEmail.getText().toString().trim();
                final String password = inputPassword.getText().toString().trim();
                if(TextUtils.isEmpty(email))
                {
                    Toast.makeText(getApplicationContext(),"Enter email address ",Toast.LENGTH_LONG).show();
                    inputEmail.requestFocus();
                    return;

                }
                if(TextUtils.isEmpty(password))
                {
                    Toast.makeText(getApplicationContext(),"Enter password ",Toast.LENGTH_LONG).show();
                    inputPassword.requestFocus();
                    return;
                }
                if(password.length()<6)
                {
                    Toast.makeText(getApplicationContext(),"password too short enter minimum 6 characters  ",Toast.LENGTH_LONG).show();
                    return;

                }
                if(k==0) {

                    mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(Registration.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (!task.isSuccessful()) {
                                Toast.makeText(Registration.this, "Authentication failed ", Toast.LENGTH_LONG).show();

                            }
                            if (task.isSuccessful()) {
                                mAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful())
                                        {
                                            Toast.makeText(Registration.this, " you are registered...please Check your email for verification ", Toast.LENGTH_LONG).show();
                                            head.setText("Log in");
                                            click.setText("Not registered? Click here to register ");
                                            k=1;
                                            //finish();

                                        }
                                        else
                                        {
                                            Toast.makeText(Registration.this,task.getException().getMessage(), Toast.LENGTH_LONG).show();


                                        }

                                    }
                                });



                            }

                        }
                    });
                }
                else if(k==1)
                {
                    mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(Registration.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful())
                            {
                                if(password.length()<6)
                                {
                                    inputPassword.setError("password minimun length should be more than 6 characters");

                                }
                                else
                                {
                                    Toast.makeText(Registration.this,"Incorrect username or password", Toast.LENGTH_LONG).show();
                                    inputPassword.setText("");
                                }
                            }
                            else
                            {
                                if(mAuth.getCurrentUser().isEmailVerified())
                                {
                                    Intent intent = new Intent(Registration.this,HomePage.class);
                                    startActivity(intent);
                                }
                                else
                                {
                                    Toast.makeText(Registration.this,"Please verify your email address",Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                    });

                }


            }
        });

        findViewById(R.id.changeToLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                details = findViewById(R.id.detailCardLayout);
                details.startAnimation(animBlink);


            }
        });

        // load animation

        animBlink = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.blink);
        animBlink.setAnimationListener(this);

    }

    @Override
    public void onAnimationStart(Animation animation) {

        if(k==0)
        {
//            head = findViewById(R.id.headingTextView);
//            next = findViewById(R.id.nextButtonView);
//            click = findViewById(R.id.changeToLogin);
            next.setText("Log in");
            head.setText("Log in");
            click.setText("Not registered? click to Sign in");
            k++;

        }

        else if(k==1)
        {
//            head = findViewById(R.id.headingTextView);
//            toLogin = findViewById(R.id.nextButtonView);
//            infoTextView = findViewById(R.id.changeToLogin);

            next.setText("Register");
            head.setText("Sign in");
            click.setText("Already registered? click to Log in");
            k--;

        }


    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
