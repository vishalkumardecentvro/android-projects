package com.example.try_efm_3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class HomePage extends baseClass {

    String content;
    int number;
    String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        ListView donation = findViewById(R.id.donationList);

        final ArrayList<String> items = new ArrayList<String>();

        // adding items to list

        items.add("Clothes");
        //items.add("Food");
        items.add("Stationary items");
        items.add("Medical Equipments");
        items.add("Sports equipments");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,items);
        donation.setAdapter(arrayAdapter);

        donation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(items.get(i)=="Clothes")
                {
                    content = "Clothes";
                    number = 0;
                    Intent intent = new Intent(getApplicationContext(), booksSection.class);
                    intent.putExtra("clothes key",content);
                    value = String.valueOf(number);
                    intent.putExtra("child value",value);
                    startActivity(intent);

                }
                else if(items.get(i)=="Stationary items")
                {
                    content = "Stationary items";
                    number = 1;
                    Intent intent = new Intent(getApplicationContext(), booksSection.class);
                    intent.putExtra("clothes key",content);
                    value = String.valueOf(number);
                    intent.putExtra("child value",value);
                    startActivity(intent);
                }
                else if(items.get(i)=="Medical Equipments")
                {
                    content = "Medical Equipments";
                    number = 2;
                    Intent intent = new Intent(getApplicationContext(), booksSection.class);
                    intent.putExtra("clothes key",content);
                    value = String.valueOf(number);
                    intent.putExtra("child value",value);
                    startActivity(intent);
                }
                else if(items.get(i)=="Sports equipments")
                {
                    content = "Sports equipments";
                    number = 3;
                    Intent intent = new Intent(getApplicationContext(), booksSection.class);
                    intent.putExtra("clothes key",content);
                    value = String.valueOf(number);
                    intent.putExtra("child value",value);

                    startActivity(intent);
                }
            }
        });

    }
}
