package com.example.try_efm_3;

public class itemList {
    private int mImageResource;
    private int mImageDelete;
    private int mEditImage;
    private String item_name;
    private String item_Rating;
    private String item_Qty;
    public itemList(int imageResource,int image,int edit, String text1, String text2, String text3)
    {
        mImageResource = imageResource;
        mImageDelete = image;
        mEditImage = edit;
        item_name = text1;
        item_Rating = text2;
        item_Qty = text3;


    }

    public void changeText1(String text)
    {
        item_name = text;
    }

    public int getImageResource()
    {
        return mImageResource;

    }
    public int getmImageDelete()
    {
        return mImageDelete;

    }
    public int getmEditImage()
    {
        return mEditImage;

    }

    public String getItem_name() {
        return item_name;
    }

    public String getItem_Rating() {
        return item_Rating;
    }

    public String getItem_Qty() {
        return item_Qty;
    }
}
