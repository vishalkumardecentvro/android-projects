package com.example.try_efm_3;

import com.google.firebase.database.Exclude;

public class ngo_information {

    private String NgoName, Details, Location, DocumentId;


    public ngo_information(String Detail, String ngoName, String Location) {
        this.Details = Detail;
        this.Location = Location;
        this.NgoName = ngoName;
    }

    // firestore always need empty constructor

    public ngo_information() {
    }


    @Exclude
    public String getDocumentId() {
        return DocumentId;
    }

    public void setDocumentId(String documentId) {
        DocumentId = documentId;
    }

    public String getDetails() {
        return Details;
    }

    public String getNgoName() {
        return NgoName;
    }

    public String getLocation() {
        return Location;
    }
}
