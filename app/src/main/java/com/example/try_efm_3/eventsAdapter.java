package com.example.try_efm_3;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class eventsAdapter extends FragmentStatePagerAdapter {

    int tabCount;


    public eventsAdapter (FragmentManager fm, int numOfTabs)
    {
        super(fm);
        this.tabCount = numOfTabs;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new events_fragment();
            case 1:
                return  new event_registered_fragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 0;
    }
}
