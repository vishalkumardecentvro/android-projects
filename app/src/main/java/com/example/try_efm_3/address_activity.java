package com.example.try_efm_3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class address_activity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private EditText houseNumber;
    private EditText roadName;
    private EditText landmark,pinCode;
    private EditText donarName;
    private EditText donarNumbr;
    private EditText donarAlternateNumber;
    private long ID;

    String Adddress,AlternateNumber,land,sendID,contactNumber,cityName,address_to_database,contactDeatail_to_database,show_add,choosedNGO,ItemID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_activity);


        houseNumber = findViewById(R.id.house_name);
        roadName = findViewById(R.id.road_name);
        landmark = findViewById(R.id.landmark);
        donarNumbr =findViewById(R.id.donar_number);
        donarAlternateNumber = findViewById(R.id.donar_alternate_number);
        donarName =findViewById(R.id.donar_name);
        pinCode = findViewById(R.id.pin_code);

        loadData(); // this is used to load data which is stored

        Intent tech_fest = getIntent();
        choosedNGO = tech_fest.getStringExtra("name");


        final Spinner city = findViewById(R.id.city_spinner);


        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.city,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_gallery_item);
        city.setAdapter(adapter);
        city.setOnItemSelectedListener(this);




        findViewById(R.id.continueButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String house_number = houseNumber.getText().toString().trim();
                String road_name = roadName.getText().toString().trim();
                String donar_number = donarNumbr.getText().toString();
                String donar_name = donarName.getText().toString().trim();
                String pin_code = pinCode.getText().toString();
                String alternate_number = donarAlternateNumber.getText().toString();



                if(TextUtils.isEmpty(house_number))
                    houseNumber.requestFocus();

                else if(TextUtils.isEmpty(road_name))
                    roadName.requestFocus();

                else if(TextUtils.isEmpty(donar_name))
                    donarName.requestFocus();

                else if(TextUtils.isEmpty(pin_code) || pin_code.length()<6) // pin code check
                {
                    Toast.makeText(address_activity.this,"Please enter valid pin code",Toast.LENGTH_LONG).show();
                    pinCode.requestFocus();
                }

                else if(TextUtils.isEmpty(donar_number) || donar_number.length()<10) // number check
                {
                    Toast.makeText(address_activity.this,"Please enter valid mobile number",Toast.LENGTH_LONG).show();
                    donarNumbr.requestFocus();
                }


                else if(alternate_number.length()<10 && alternate_number.length()>0) // alternate number check
                {
                    Toast.makeText(address_activity.this,"Please enter valid alternate mobile number",Toast.LENGTH_LONG).show();
                    donarAlternateNumber.requestFocus();
                }


                else
                {

                    ID = System.currentTimeMillis();
                    sendID = String.valueOf(ID);

                    if(landmark.getText().toString() == "")
                        land = "NO Landmark provided";
                    else
                        land = landmark.getText().toString();


                    Adddress = houseNumber.getText().toString()+", "+roadName.getText().toString()
                            +", "+cityName+", "+pinCode.getText().toString()+", "+land;



                    if(donarAlternateNumber.getText().toString() == "")
                        AlternateNumber = "NO alternate number";
                    else
                        AlternateNumber = donarAlternateNumber.getText().toString();

                    contactNumber = donarName.getText().toString()+","+donarNumbr.getText().toString()+","+AlternateNumber;

                    Log.i("addr","\n"+Adddress+"\n"+sendID+"\n"+contactNumber);



                    ItemID = sendID;
                    address_to_database =  Adddress;
                    contactDeatail_to_database = donarName.getText().toString()+","+donarNumbr.getText().toString()+","+donarAlternateNumber.getText().toString();
                    show_add = roadName.getText()+", "+pinCode.getText();


                    SharedPreferences ngo_sharedPreferences = getSharedPreferences("choosedNGO",MODE_PRIVATE);
                    String real = (ngo_sharedPreferences.getString("saved_ngo_name",""));



                    if(real == null )
                    {
                        Toast.makeText(address_activity.this,"Please select NGO ",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(address_activity.this,NGO_layout.class));
                    }
                    else
                    {
                        saveAddress();  // this is used to save address even when the app will be closed
                        startActivity(new Intent(address_activity.this,baseClass.class));

                    }



                }


            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(address_activity.this,NGO_layout.class));
    }

    public void saveAddress()
    {

        SharedPreferences sharedPreferences = getSharedPreferences("address",MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("house_no",houseNumber.getText().toString());
        editor.putString("road_name",roadName.getText().toString());
        editor.putString("pin_code",pinCode.getText().toString());
        editor.putString("customer_name",donarName.getText().toString());
        editor.putString("contact_no",donarNumbr.getText().toString());
        editor.putString("landmark",landmark.getText().toString());
        editor.putString("alternate_number",donarAlternateNumber.getText().toString());

        editor.putString("ID",ItemID);
        editor.putString("final_address",address_to_database);
        editor.putString("show address",show_add);
        editor.putString("contactDetails",contactDeatail_to_database);

        editor.apply();


    }

    public void loadData()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("address",MODE_PRIVATE);
        houseNumber.setText(sharedPreferences.getString("house_no",""));
        roadName.setText(sharedPreferences.getString("road_name",""));
        pinCode.setText(sharedPreferences.getString("pin_code",""));
        donarName.setText(sharedPreferences.getString("customer_name",""));
        donarNumbr.setText(sharedPreferences.getString("contact_no",""));
        landmark.setText(sharedPreferences.getString("landmark",""));
        donarAlternateNumber.setText(sharedPreferences.getString("alternate_number",""));

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        cityName = text;

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }
}