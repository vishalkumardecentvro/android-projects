package com.example.try_efm_3;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterExample extends RecyclerView.Adapter<AdapterExample.ExampleViewHolder> {
    private ArrayList<itemList> mExampleList;
    private OnItemClicklistener mListener;



    public interface OnItemClicklistener
    {
        void onItemClick(int position);
        void onDeleteClick(int position);
        void onEditClick(int position);

    }

    public void setOnItemClickListener(OnItemClicklistener listener)
    {
        mListener = listener;

    }


    public static class ExampleViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView mimageView;
        public ImageView mImageDelete;
        public TextView mTextView1;
        public TextView mTextview2;
        public TextView mTextview3;
        public ImageView mDeleteImage;
        public ImageView mEditImage;

        public ExampleViewHolder(@NonNull View itemView, final OnItemClicklistener listener) {
            super(itemView);
            mimageView = itemView.findViewById(R.id.image);
            mImageDelete = itemView.findViewById(R.id.deleteIcon);
            mTextView1 = itemView.findViewById(R.id.textView);
            mTextview2 = itemView.findViewById(R.id.textView2);
            mTextview3 = itemView.findViewById(R.id.textViewThird);
            mDeleteImage = itemView.findViewById(R.id.deleteIcon);
            mEditImage = itemView.findViewById(R.id.editIcon);

            mDeleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null)
                    {
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION)
                        {

                            //openDialog();
                            listener.onDeleteClick(position);

                        }
                    }

                }
            });

            mEditImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null)
                    {
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION)
                        {

                            //openDialog();
                            listener.onEditClick(position);

                        }
                    }
                }
            });

        }
    }



    public AdapterExample(ArrayList<itemList>exampleList)
    {
        mExampleList = exampleList;
    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,parent,false );
        ExampleViewHolder evh = new ExampleViewHolder(v,mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {
        itemList currentitem = mExampleList.get(position);
        holder.mimageView.setImageResource(currentitem.getImageResource());
        holder.mImageDelete.setImageResource(currentitem.getmImageDelete());
        holder.mEditImage.setImageResource(currentitem.getmEditImage());
        holder.mTextView1.setText(currentitem.getItem_name());
        holder.mTextview2.setText(currentitem.getItem_Rating());
        holder.mTextview3.setText(currentitem.getItem_Qty());
    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }

}
