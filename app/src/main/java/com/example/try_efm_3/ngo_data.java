package com.example.try_efm_3;

public class ngo_data {

    private String NgoName, Details;


    public ngo_data(String Detail, String ngoName) {
        this.Details = Detail;
        this.NgoName = ngoName;
    }

    public String getNgoName() {
        return NgoName;
    }

    public String getDetails() {
        return Details;
    }
}
