package com.example.try_efm_3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class NGO_layout extends AppCompatActivity {

    TextView nameAndLocation,ngoName;


    private FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
    private DocumentReference ngo_Detail = mFirestore.collection("ngo details").document("ufV3rQDp6CdlJ3OqV1TW");
    private CollectionReference ngo_Collection = mFirestore.collection("ngo details");
    private static final String TAG = "booksSection";

    ArrayList<ngo_data> ngo_inf = new ArrayList<>();

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mrecyclerView;
    private ngo_adapter mAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_n_g_o_layout);

        nameAndLocation = findViewById(R.id.ngoNameLocation);
        ngoName = findViewById(R.id.NumberOfItems);

        fetch_ngo_data();
        loadData_ngo_name();




        findViewById(R.id.address_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name  = ngoName.getText().toString();

                if(TextUtils.isEmpty(name))
                    Toast.makeText(NGO_layout.this, "Please select NGO and then proceed", Toast.LENGTH_SHORT).show();

                else
                {
                    saveNGOname(name);
                    startActivity(new Intent(NGO_layout.this,address_activity.class));
                }


            }
        });


    }

    public void saveNGOname(String name)
    {
        SharedPreferences sharedPreferences = getSharedPreferences("choosedNGO",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("saved_ngo_name",name);
        editor.apply();
    }

    public void loadData_ngo_name()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("choosedNGO",MODE_PRIVATE);
        ngoName.setText(sharedPreferences.getString("saved_ngo_name",""));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(NGO_layout.this,booksSection.class));
    }

    public void fetch_ngo_data()
    {
        final loading_dialog load_dialog = new loading_dialog(NGO_layout.this);
        load_dialog.startLoading();
        ngo_Collection.get().addOnSuccessListener( new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for(QueryDocumentSnapshot documentSnapshot:queryDocumentSnapshots)
                {
                    ngo_information ngo_detail = documentSnapshot.toObject(ngo_information.class);
                    ngo_detail.setDocumentId(documentSnapshot.getId());

                    String ngoID = ngo_detail.getDocumentId();
                    String name = ngo_detail.getNgoName();
                    String location = ngo_detail.getLocation();
                    String detail = ngo_detail.getDetails();
                    name = name+"\n"+location;


                    ngo_inf.add(new ngo_data(detail,name));
                    showData();


                }
                load_dialog.dismissDialog();



            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(NGO_layout.this, "Error loading information", Toast.LENGTH_SHORT).show();
                Log.d(TAG,e.getMessage());

            }
        });


    }

    public void showData()
    {
        mrecyclerView = findViewById(R.id.ngoCardLayout_recycler_view);
        mrecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        mAdapter = new ngo_adapter(ngo_inf);
        mrecyclerView.setLayoutManager(mLayoutManager);
        mrecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new ngo_adapter.OnItemClicklistener() {



            @Override
            public void applyTexts(String selectedNgoName) {
                ngoName.setText(selectedNgoName);

            }
        });

    }

}