package com.example.try_efm_3;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RetrofitInterface {

    @POST("/newOrder")
    Call<Void> executeOrder (@Body HashMap<String,String> map);

}
