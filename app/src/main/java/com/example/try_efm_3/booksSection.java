package com.example.try_efm_3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Rating;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.common.reflect.TypeToken;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
//import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Document;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class booksSection extends baseClass implements AdapterView.OnItemSelectedListener   {

    ViewFlipper v_flipper;
    int count,value=0;
    EditText qty,Rating;
    TextView head,ngodata,ngoName,ItemSum;
    String summarry="",good;
    Button send,flip,backFlip;
    int mass_count = 0;

    private static final String TAG = "booksSection";

    String names[] = {"Clothes","Stationary items","Medical Equipments","Sports equipments"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_section);
        v_flipper = findViewById(R.id.v_flipper);
        head = findViewById(R.id.heading);
        qty= findViewById(R.id.quantitytextView);
        Rating = findViewById(R.id.condition);
        send = findViewById(R.id.send);
        ItemSum = findViewById(R.id.NumberOfItems);

        loadItemCount();

        findViewById(R.id.bottomButtonOne).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomsheetClass bottomSheet = new BottomsheetClass();
                bottomSheet.show(getSupportFragmentManager(),"bottomSheet");
            }
        });

        findViewById(R.id.bottomButtonTwo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomsheetClass_two bottomSheet = new BottomsheetClass_two();
                bottomSheet.show(getSupportFragmentManager(),"bottomSheet");
            }
        });

        findViewById(R.id.bottomButtonThree).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomsheetClass_three bottomSheet = new BottomsheetClass_three();
                bottomSheet.show(getSupportFragmentManager(),"bottomSheet");
            }
        });



        Intent fetch = getIntent();
        String cloth = fetch.getStringExtra("clothes key");
        String number = fetch.getStringExtra("child value");

        /* this check condition has been specially put up for Empty cart activity...because when user didnt have selected any item for donation
          and when he click cart it goes to Empty cart... and when add item button is pressed clothes key and child value both becomes null..
          because these values are passed from home section activity...coming back from any other activity will make values null;*/

        if(TextUtils.isEmpty(cloth))
            head.setText("Clothes");

        else
            head.setText(cloth);



        if(TextUtils.isEmpty(number))
            value = 0;

        else
            value = Integer.parseInt(number); // converts string to integer


        v_flipper.setDisplayedChild(value);
        count = value;
        ngodata = findViewById(R.id.ngoInfo);
        ngoName = findViewById(R.id.ngoNameLocation);
        flip = findViewById(R.id.choose);
        backFlip = findViewById(R.id.chooseBack);




        findViewById(R.id.chooseNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(booksSection.this,NGO_layout.class));

            }
        });



        findViewById(R.id.Info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mExampleList.add(new itemList(R.drawable.ic_android,R.drawable.delete,R.drawable.create,good,"Rating- "+Rating.getText().toString(),"Quantity- "+qty.getText().toString()));


//                baseClass.listItems.add(good);
//                baseClass.RatingList.add(Rating.getText().toString());
//                baseClass.QtyList.add(qty.getText().toString());
                mass_count += mExampleList.size();  // incrementing mass count by 1 using short hand operator

                saveItemCount(mass_count);
                summarry = mass_count+" Item Added";
                ItemSum.setText(summarry);

            }
        });







//        StorageReference islandRef = storageRef.child("ngo2.jpg");
//
//        final long ONE_MEGABYTE = 1024 * 1024;
//        islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
//            @Override
//            public void onSuccess(byte[] bytes) {
//                // Data for "images/island.jpg" is returns, use this as needed
//                setImageViewWithByteArray(ngoImage,bytes);
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception exception) {
//                // Handle any errors
//            }
//        });



        final Spinner kapda = findViewById(R.id.spinner1);


        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.clothes,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_gallery_item);
        kapda.setAdapter(adapter);
        kapda.setOnItemSelectedListener(this);

        // stationary spinner layout


        final ArrayAdapter<CharSequence> adapterOne = ArrayAdapter.createFromResource(this,R.array.items,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_gallery_item);

        // Medical spinner layout

        final ArrayAdapter<CharSequence> adapterTwo = ArrayAdapter.createFromResource(this,R.array.medical,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_gallery_item);

        // sports eqipments

        final ArrayAdapter<CharSequence> adapterThree = ArrayAdapter.createFromResource(this,R.array.sports,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_gallery_item);

        flip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(count>=names.length-1)  // names length =4;
                {
                    count=-1; // short hand operator //
                }
                count++;
                head.setText(names[count]);
                if(count == 0)
                    kapda.setAdapter(adapter);
                else if(count == 1)
                    kapda.setAdapter(adapterOne);
                else if(count == 2)
                    kapda.setAdapter(adapterTwo);
                else
                    kapda.setAdapter(adapterThree);

            }
        });

        backFlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(count==0)
                {
                    count = names.length;
                    //v_flipper.stopFlipping();
                }
                count--;
                head.setText(names[count]);
                if(count == 0)
                    kapda.setAdapter(adapter);
                else if(count == 1)
                    kapda.setAdapter(adapterOne);
                else if(count == 2)
                    kapda.setAdapter(adapterTwo);
                else
                    kapda.setAdapter(adapterThree);

            }
        });



    }

    public void saveItemCount(int mass)
    {
        SharedPreferences item_number = getSharedPreferences("count",MODE_PRIVATE);
        SharedPreferences.Editor edit = item_number.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mExampleList);
        edit.putString("task list",json);
        edit.apply();

    }

    public void loadItemCount()
    {

        if(mExampleList.size()!=0)
        {
            SharedPreferences item_number = getSharedPreferences("count",MODE_PRIVATE);
            Gson gson = new Gson();
            String json = item_number.getString("task list",null);
            Type type = new TypeToken<ArrayList<itemList>>() {}.getType();
            mExampleList = gson.fromJson(json,type);
            if(mExampleList == null)
                mExampleList = new ArrayList<>();
            mass_count = mExampleList.size();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        good = text;
        good = good.replaceAll("\\s", "");
        //Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }



    public static void setImageViewWithByteArray(ImageView view, byte[] data) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        view.setImageBitmap(bitmap);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(booksSection.this,HomePage.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.out:
            {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(booksSection.this,Registration.class));

                return true;

            }
            case R.id.cart:
            {
                String item_count = ItemSum.getText().toString();


                if(TextUtils.equals(item_count,"No item added"))
                    startActivity(new Intent(booksSection.this,Empty_cart.class));
                else
                    startActivity(new Intent(booksSection.this,baseClass.class));


            }




            default:
                return false;

        }
    }


}
