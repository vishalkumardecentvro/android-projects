package com.example.try_efm_3;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;

public class loading_dialog {

    Activity activity;
    AlertDialog dialog;

    loading_dialog(Activity myActivity)
    {
        activity = myActivity;
    }

    void startLoading()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.loading_dialog,null));
        builder.setCancelable(false);

        dialog = builder.create();
        dialog.show();
    }

    void dismissDialog()
    {
        dialog.dismiss();
    }

}
