package com.example.try_efm_3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

public class ExampleDialogCorrectingValues extends AppCompatDialogFragment {
    private EditText editTextRating,editTextQuantity;
    private ExampleDialogCorrectingValuesListener Listener;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog_edit,null);
        builder.setView(view).setTitle("Edit your Donation Item").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String Rating = editTextRating.getText().toString();
                String Quantity = editTextQuantity.getText().toString();
                Listener.applyTexts(Rating,Quantity);

            }
        });

        editTextQuantity = view.findViewById(R.id.correctedQuantity);
        editTextRating = view.findViewById(R.id.correctedRating);


        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            Listener = (ExampleDialogCorrectingValuesListener) context;

        }catch (ClassCastException e)
        {
            throw new ClassCastException(context.toString()+"must implement example dialog Listener");
        }

    }

    public interface ExampleDialogCorrectingValuesListener
    {
        void applyTexts(String Rating,String Quantity);

    }
}
