package com.example.try_efm_3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Rating;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.lang.Integer.parseInt;

public class baseClass extends AppCompatActivity implements ExampleDialog.ExampleDialogListener, ExampleDialogCorrectingValues.ExampleDialogCorrectingValuesListener{

    static ArrayList<itemList>mExampleList = new ArrayList<>();
//    static ArrayList<String> listItems = new ArrayList<String>();
//    static ArrayList<String> RatingList = new ArrayList<String>();
//    static ArrayList<String> QtyList = new ArrayList<String>();
    int posn,itemCountTrack=0,posn2;
    


    private RecyclerView  mRecyclerView;
    private AdapterExample mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public TextView nameOfNGO,customer_add;

    private Retrofit retrofit;
    private RetrofitInterface retrofitInterface;
    private String BASE_URL = "http://10.0.2.2:3000";




    Button send;
    public TextView textRating,textQuantity;
    String Rat,Qty,RatingToFirebase="",QuantityToFirebase="",NameToFirebase="",selectedNgoName;
    private FirebaseFirestore mFirestore =  FirebaseFirestore.getInstance();


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_class);

        //createExampleList();

        buildRecyclerView();
        send = findViewById(R.id.send);
        textRating = findViewById(R.id.textView2);
        textQuantity = findViewById(R.id.textViewThird);
        nameOfNGO = findViewById(R.id.ngoNameToDonate);
        customer_add = findViewById(R.id.address_summary);

        load_Donation_items();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofitInterface = retrofit.create(RetrofitInterface.class);



        findViewById(R.id.edit_address).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(baseClass.this,address_activity.class));

            }
        });



        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Log.i("send data",customer_address.get(0)+"\n"+customer_NGO.get(0))
                sendData_to_Database();
                Toast.makeText(baseClass.this, "Data logged", Toast.LENGTH_SHORT).show();
            }

        });





    }

    public void removeitem(int position)
    {
        mExampleList.remove(position);
        mAdapter.notifyItemRemoved(position);

    }

    public void changeItem(int position,String text)
    {
        mExampleList.get(position).changeText1(text);
        mAdapter.notifyItemChanged(position);


    }

//    public void createExampleList()
//    {
//        mExampleList = new ArrayList<>();
//
//        for(int i=0;i<listItems.size();i++)
//            mExampleList.add(new itemList(R.drawable.ic_android,R.drawable.delete,R.drawable.create,listItems.get(i),"Rating = "+RatingList.get(i),"Quantity ="+QtyList.get(i)));
//
//    }

    public void buildRecyclerView()
    {
        mRecyclerView = findViewById(R.id.item);
        textRating = findViewById(R.id.textView2);
        textQuantity = findViewById(R.id.textViewThird);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new AdapterExample(mExampleList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new AdapterExample.OnItemClicklistener() {
            @Override
            public void onItemClick(int position) {
                changeItem(position,"Clicked");  // in future we can use to change a text in card layout, currently nt used

            }

            @Override
            public void onDeleteClick(int position) {
                posn = position;
                openDialog();


            }

            @Override
            public void onEditClick(int position) {  // this edit dialog changes rating and quantity
                posn2 = position;
                openEditDialog();


            }
        });


    }
    public void openEditDialog() // this opens dialog correcting values class
    {
        ExampleDialogCorrectingValues exampleDialog = new ExampleDialogCorrectingValues();
        exampleDialog.show(getSupportFragmentManager(),"Edit example dialog");
    }

    @Override
    public void applyTexts(String Rating, String Quantity) {
        Log.i("Dialog Rating =",Rating);
        mExampleList.get(posn).changeText1(Rating);

        Rat = Rating;
        Qty = Quantity;

    }

    public void openDialog()
    {
        ExampleDialog exampleDialog = new ExampleDialog();
        exampleDialog.show(getSupportFragmentManager(),"example Dialog");

    }

    @Override
    public void onYesClicked() {
        removeitem(posn);
        itemCountTrack++;  // plan to update item added text view

    }


    private void load_Donation_items()
    {
        SharedPreferences ngo_sharedPreferences = getSharedPreferences("choosedNGO",MODE_PRIVATE);
        SharedPreferences sharedPreferences = getSharedPreferences("address",MODE_PRIVATE);

        String customer_address = (sharedPreferences.getString("show address",""));


        if(customer_address == null)
            customer_address = "Please enter pick up details";

        else
            customer_address = "Pick up from -: "+ customer_address;

        customer_add.setText(customer_address);


        String real = (ngo_sharedPreferences.getString("saved_ngo_name",""));

        if(real == null)
            nameOfNGO.setText("Please select"+"\n"+ "NGO");

        else
            nameOfNGO.setText(real);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(baseClass.this,booksSection.class));
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.out:
            {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(baseClass.this,Registration.class));
                //Toast.makeText(getApplicationContext(), "Settings clicked ", Toast.LENGTH_SHORT).show();
                return true;

            }
            case R.id.cart:
            {
                //startActivity(new Intent(baseClass.this,Registration.class));


                return true;
            }


            default:
                return false;

        }
    }



    public void sendData_to_Database() {

        HashMap<String,String> usermap = new HashMap<>();
        String name="",rate="",qty="";


        usermap.put("Rating",RatingToFirebase);
        usermap.put("typeOfItem",NameToFirebase);
        usermap.put("quantity",QuantityToFirebase);
        // contact details
        SharedPreferences sharedPreferences = getSharedPreferences("address",MODE_PRIVATE);
        String Address = sharedPreferences.getString("final_address","not provided");
        String sendID = sharedPreferences.getString("ID","0000");
        String contacts = sharedPreferences.getString("contactDetails","0000");

        // NGO name
        SharedPreferences ngo_sharedPreferences = getSharedPreferences("choosedNGO",MODE_PRIVATE);
        String sel = ngo_sharedPreferences.getString("saved_ngo_name","not provided");

        for(int i=0;i<mExampleList.size();i++)
        {
            name += mExampleList.get(i).getItem_name()+" ";
            rate += mExampleList.get(i).getItem_Rating()+" ";
            qty += mExampleList.get(i).getItem_Qty()+" ";

        }

        String date = new SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(new Date());
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        String currentTime = String.valueOf(hour+""+minute+""+second);

        Random rand = new Random();
        // Generate random integers in range 0 to 999999
        int rand_int1 = rand.nextInt(1000000);


        String id = currentTime+"-"+date+"-"+rand_int1;


        usermap.put("rating",rate);
        usermap.put("typeOfItem",name);
        usermap.put("quantity",qty);

        usermap.put("address",Address);
        usermap.put("donarNameNumberAlternate",contacts);
        usermap.put("id",id);
        usermap.put("ngoChoosed",sel);

        Call<Void> call = retrofitInterface.executeOrder(usermap);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200 )
                {
                    Toast.makeText(baseClass.this,"Pick up order placed sucesssfully!!!",Toast.LENGTH_LONG).show();
                }
                else if(response.code() == 400 ){
                    Toast.makeText(baseClass.this,"Error ocured",Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

                Toast.makeText(baseClass.this, t.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        });


//        mFirestore.collection("orders").add(usermap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//            @Override
//            public void onSuccess(DocumentReference documentReference) {
//                Toast.makeText(baseClass.this,"Donation request complete, thanks for donating", Toast.LENGTH_LONG).show();
//
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                String error = e.getMessage();
//                Toast.makeText(baseClass.this,error,Toast.LENGTH_SHORT).show();
//            }
//        });

    }


}
