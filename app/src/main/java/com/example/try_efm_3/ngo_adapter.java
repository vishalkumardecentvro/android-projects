package com.example.try_efm_3;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ngo_adapter extends RecyclerView.Adapter<ngo_adapter.ExampleViewHolder > {

    private ArrayList<ngo_data> mExampleList;
    private OnItemClicklistener mListener;

    public interface OnItemClicklistener
    {
        void applyTexts(String selectedNgoName);
    }


    public void setOnItemClickListener(OnItemClicklistener Listener)
    {
        mListener = Listener;
    }



    public static class ExampleViewHolder extends RecyclerView.ViewHolder
    {
        public TextView ngo_name;
        public TextView ngo_detail;
        public Button select;


        public ExampleViewHolder(@NonNull final View itemView, final OnItemClicklistener Listener) {
            super(itemView);

            ngo_name = itemView.findViewById(R.id.ngoNameLocation);
            ngo_detail = itemView.findViewById(R.id.ngoInfo);
            select = itemView.findViewById(R.id.choose_ngo);


            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final String name =  ngo_name.getText().toString();

                    if(Listener != null)
                    {

                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION)
                        {

                            Listener.applyTexts(name);

                        }
                    }
                }
            });


        }
    }



    public ngo_adapter (ArrayList<ngo_data> exampleList)
    {
        mExampleList = exampleList;

    }

    @NonNull
    @Override
    public ngo_adapter.ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ngo_view ,parent,false );
        ExampleViewHolder evh = new ExampleViewHolder(v,mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull ngo_adapter.ExampleViewHolder holder, int position) {

        ngo_data currentItem = mExampleList.get(position);
        holder.ngo_name.setText(currentItem.getNgoName());
        holder.ngo_detail.setText(currentItem.getDetails());
    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }
}
