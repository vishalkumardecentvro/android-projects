Idea -> This projects aim to solve the problem of reusing the items. The user can donate the items from this application by making a donation request to NGO and NGO will distribute the items to people who are in need of it.


<img src="imageRepo/Ngo-customer-side-Part-1.gif" width = "250">
<img src="imageRepo/Ngo-customer-side-part-2.gif" width = "250">

<img src="imageRepo/1_authPage.png" width = "250">
<img src="imageRepo/2_authCredential.png" width = "250">
<img src="imageRepo/3_menueOption.png" width = "250">
<img src="imageRepo/4_itemDetail.png" width = "250">
<img src="imageRepo/5_chooseItem.png" width = "250">
<img src="imageRepo/6_addItem.png" width = "250">
<img src="imageRepo/7_typeBottomsheet.png" width = "250">
<img src="imageRepo/8_ratingBottomsheet.png" width = "250">
<img src="imageRepo/9_quantityBottomsheet.png" width = "250">
<img src="imageRepo/10_ngoSelection.png" width = "250">
<img src="imageRepo/11_addressDetail.png" width = "250">
<img src="imageRepo/12_ItemSummary.png" width = "250">
<img src="imageRepo/13_deleteOption.png" width = "250">
<img src="imageRepo/14_logOut.png" width = "250">
<img src="imageRepo/15_emptyAddress.png" width = "250">




